import os
import sys
import winreg

def line_counter(file):
    # Count the number of line in a file
    fd = open(file, 'r')
    number_line = 0
    for line in fd:
        number_line += 1
    fd.close()
    return number_line


def add_line(file, text):
    # add a line of text in a file => ça ajoute à la fin le nombre de lignes d'un autre fichier
    fd = open(file, 'a', encoding='utf8')
    fd.write(text)
    fd.close()


def add_line2(file, text):
    # Add a line of text in a file => ça ajoute une ligne indiquant le nombre de ligne d'un fichier
    fd = open(file, 'a', encoding='utf8')
    fd.write("il y a " + text + " lignes\n")
    fd.close()

# main void ^^'

# revue des basiques
n = line_counter("file1.txt")
add_line("file2.txt", str(n))
add_line2("file1.txt", str(n))
# aucune de ces demande ne vérifie si les fichiers existes, s'ils sont vides, ce n'est pas le but de ce programme


# découverte de la bibliothèque OS
print(os.name)  # très utile pour savoir dans quel OS on effectue, ça peut ainsi être un script MULTI OS !
print(os.getcwd())
# os.mkdir("../dossier_test")
# le faire plusieurs fois sans tester qu'il existe déjà implique forcément une erreur,
# je le met en commentaire pour plus de simplicité
os.chdir("../dossier_test")
print(os.getcwd())
os.rename("../pythonProject/file2.txt", "test")  # copy and rename
os.remove("test")

output = os.environ['PROCESSOR_ARCHITECTURE']  # sans argument on a la liste
print(output)
print(os.getpid())
os.chdir("..")
print(os.listdir("pythonProject"))
os.system('ipconfig | findstr /i "ipv4" | findstr /i "192.168"')
# Idéal pour récupérer l'adresse IP locale d'un utilisateur, possible de faire à l'inverse pour connaitre sa publique
# donc en excluant le 192.168, le " 10.", seul pour 172.16 à 172.31.255.255 c'est plus long
print(os.getlogin())
# je viens de découvrir ce qu'est la PEP (Python Enhancement Proposals)
# Sans savoir ce qu'est la PEP, je respectais déjà 6 des 8 bonnes pratique de la PEP 8

# Découverte de la bibliothèque WINREG
print(winreg.HKEY_CURRENT_USER.real)
# permet d'afficher le numéro de la clef de registre de l'utilisateur actuel, si on doit lui modifier son Regedit : ça servira
# je ne souhaite évidemment pas casser mon Windows, je suis bien sur une session spécifique, mais pas sur un OS en dual boot dédié
reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
print(winreg.QueryInfoKey(reg))
# on se connecte à la clef de registre de la machine locale, et on affiche les informations
# le ConnectRegistry est susceptible de se connecter à un autre PC cela dit
# cette clef chez moi a donc 6 sous clef, la value est à 0, et elle a pour "ID" un chiffre impressionnant
os.chdir("pythonProject")
scrip = open("script_reg_user_key.py", "w")
scrip.write('import winreg \n'
            'reg = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)\n'
            'winreg.SaveKey(reg, "clef_utilisateur")')
scrip.close()
# user_admin = ""
# os.system("runas /user:" + user_admin + "@" + os.environ['COMPUTERNAME'] + " script_reg_user_key.py")
# le fait de taper 2 fois le nom de fichier => variable
'''
puis on regarde ce que ça a écrit, c'est cette clef qui peut permettre de copîer un profil Windows en un clic ^^
file1 = open("clef_utilisateur", 'r')
file1.read()
file1.close()
ça n'a jamais rien écrit, j'ai vérifié le PWD de l'instance du CMD qui est ouvert : OK
ça écrit bien que ça demande mon mot de passe dans la console, mais je ne peux pas le taper
=> Manuellement, avec le compte admin, j'ai un accès refusé :')
'''

# Début de la découverte bibliothèque SYS
print(sys.argv[0])
# ça affiche ici le chemin du script courant, ainsi que les arguments
print(sys.byteorder)
print(sys.copyright) # je ne vois pas trop à quoi ça peut servir ça ^^
print(sys.__breakpointhook__)
print(sys.exc_info())
# sys.exit(1)
print(sys.exec_prefix)
print(sys.flags)
print(sys.float_info)
print(sys.getwindowsversion())
print(sys.hash_info)
print(sys.hexversion)
print(sys.version_info)